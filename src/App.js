import "./App.css";
import EventListWithHotdrink from "./with-hotdrink/TalkList";
import EventListWithoutHotdrink from "./without-hotdrink/TalkList";

function App() {
  return (
    <div className="App">
      {/* <EventListWithoutHotdrink/> */}
      <EventListWithHotdrink />
    </div>
  );
}

export default App;
