import React from "react";

class Talk extends React.Component {
  constructor(props) {
    super(props);
    this.remove = this.remove.bind(this);
    this.updateDuration = this.updateDuration.bind(this);
  }

  remove() {
    this.props.remove(this.props.index);
  }

  updateDuration(event) {
    const newValue = event.target.value;
    this.props.updateDuration(this.props.index, newValue);
  }

  render() {
    return (
      <div>
        <input type="text" readOnly value={this.props.start} />
        <input
          type="text"
          onChange={this.updateDuration}
          value={this.props.dur}
        />
        <input type="text" readOnly value={this.props.end} />
        <button
          className="move-up"
          onClick={() => this.props.moveUp(this.props.index)}
        >
          Move up
        </button>
        <button
          className="move-down"
          onClick={() => this.props.moveDown(this.props.index)}
        >
          Move down
        </button>
        <button onClick={this.remove}>Remove</button>
      </div>
    );
  }
}

export default Talk;
