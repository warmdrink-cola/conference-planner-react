import Talk from "./Talk";
import React from "react";
import { addTimes } from "../utils";

const INITIAL_START_TIME = "09:00";

class TalkList extends React.Component {
  constructor(props) {
    super(props);

    // Bind 'this' to all event handlers
    this.updateStartTime = this.updateStartTime.bind(this);
    this.insertNew = this.insertNew.bind(this);
    this.removeEvent = this.removeEvent.bind(this);
    this.moveUp = this.moveUp.bind(this);
    this.moveDown = this.moveDown.bind(this);
    this.updateDuration = this.updateDuration.bind(this);
    this.computeEventTimes = this.computeEventTimes.bind(this);

    // Set initial state
    this.state = {
      startTime: INITIAL_START_TIME,
      durations: [],
      events: this.computeEventTimes(INITIAL_START_TIME, []),
    };
  }

  // @Override
  componentDidUpdate(_prevProps, prevState) {
    const startTimeChanged = this.state.startTime !== prevState.startTime;
    const durationsChanges = this.state.durations !== prevState.durations;
    if (startTimeChanged || durationsChanges) {
      const newEvents = this.computeEventTimes(
        this.state.startTime,
        this.state.durations
      );
      this.setState({ events: newEvents });
    }
  }

  // Compute event times and return list of EventComponents
  computeEventTimes(startTime, durations) {
    const events = [];
    let prevEndTime = startTime;
    for (let i = 0; i < durations.length; i++) {
      const dur = durations[i];
      const currentEndTime = addTimes(prevEndTime, dur);
      events.push(
        <li key={i}>
          <Talk
            start={prevEndTime}
            dur={dur}
            end={currentEndTime}
            index={i}
            updateDuration={this.updateDuration}
            remove={this.removeEvent}
            moveUp={this.moveUp}
            moveDown={this.moveDown}
          />
        </li>
      );
      prevEndTime = currentEndTime;
    }
    return events;
  }

  updateDuration(index, value) {
    const newDurations = [...this.state.durations];
    newDurations[index] = value;
    this.setState({ durations: newDurations });
  }

  updateStartTime(event) {
    const newStartTime = event.target.value;
    this.setState({ startTime: newStartTime });
  }

  insertNew() {
    this.setState({ durations: [...this.state.durations, "00:30"] });
  }

  removeEvent(index) {
    const durationsCopy = [...this.state.durations];
    durationsCopy.splice(index, 1);
    this.setState({ durations: durationsCopy });
  }

  moveUp(index) {
    const durationsCopy = [...this.state.durations];
    const moved = durationsCopy.splice(index, 1);
    durationsCopy.splice(index - 1, 0, moved[0]);
    this.setState({ durations: durationsCopy });
  }

  moveDown(index) {
    const durationsCopy = [...this.state.durations];
    const moved = durationsCopy.splice(index, 1);
    durationsCopy.splice(index + 1, 0, moved[0]);
    this.setState({ durations: durationsCopy });
  }

  // @Override
  render() {
    return (
      <header className="App-header">
        <h1>Without HotDrink</h1>
        <div>
          <input
            onChange={this.updateStartTime}
            type="text"
            value={this.state.startTime}
          />
          <ol>{this.state.events}</ol>
          <button onClick={this.insertNew}>Insert new</button>
        </div>
      </header>
    );
  }
}

export default TalkList;
