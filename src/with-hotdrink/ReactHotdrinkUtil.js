/**
 * Binding an HD variable to a React component means updating the state
 * of the component whenever the variable is updated.
 */
export function bindVarToState(setState, hdComp, varName) {
  const variable = hdComp.vs[varName];
  variable.value.subscribe((change) => {
    if (change.value) {
      setState({ [varName]: change.value });
    }
  });
}

/**
 * For each variable in the HD component,
 * create a state field (with the same name) that represents the variable
 * and is updated whenever the variable is updated.
 */
export function subscribeStateToHdComponent(setState, hdComp) {
  hdComp._varRefs.forEach((ref) => {
    const refName = hdComp.variableReferenceName(ref);
    if (!ref.value) return;
    if (ref.value.owner !== ref) return;
    bindVarToState(setState, hdComp, refName);
  });
}

/**
 * Clear all subscriptions of an HD Component.
 * This should be called whenever a React component is destroyed
 * to prevent the HD component from updating the state of the destroyed component.
 */
export function removeAllSubscriptions(hdComp) {
  hdComp._vars.forEach((variable) => {
    variable._subject._subjectState.observers.forEach((obs) => {
      obs.complete();
    });
  });
}
