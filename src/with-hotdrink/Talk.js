import React from "react";
import {
  removeAllSubscriptions,
  subscribeStateToHdComponent,
} from "./ReactHotdrinkUtil";

class EventConstraintComponent extends React.Component {
  constructor(props) {
    super(props);

    // Make this component 'this' in the event listeners
    this.remove = this.remove.bind(this);
    this.updateDuration = this.updateDuration.bind(this);

    // Initial state. These fields will update immediately as a consequency of
    // subscribing to an HD Component when mounting.
    this.state = { start: -1, duration: -1, end: -1, name: "New" };
  }

  componentDidMount() {
    // Update React component state whenever the HD component updates.
    subscribeStateToHdComponent(this.setState.bind(this), this.props.hdComp);
  }

  componentWillUnmount() {
    removeAllSubscriptions(this.props.hdComp);
  }

  remove() {
    // Removes the component from the list by invoking `remove` from props.
    this.props.remove(this.props.index);
  }

  /**
   * Event listener that updates the HD variable duration.
   */
  updateDuration(event) {
    const newValue = event.target.value;
    this.props.hdComp.vs.duration.value.set(newValue);
  }

  render() {
    return (
      <div>
        <span className="event-name">{this.state.name}</span>
        <input type="text" readOnly value={this.state.start} />
        <input
          type="text"
          onChange={this.updateDuration}
          value={this.state.duration}
        />
        <input type="text" readOnly value={this.state.end} />
        <button
          className="move-up"
          onClick={() => this.props.moveUp(this.props.index)}
        >
          Move up
        </button>
        <button
          className="move-down"
          onClick={() => this.props.moveUp(this.props.index + 1)}
        >
          Move down
        </button>
        <button onClick={this.remove}>Remove</button>
      </div>
    );
  }
}

export default EventConstraintComponent;
