import EventConstraintComponent from "./Talk";
import React from "react";
import { component, defaultConstraintSystem } from "hotdrink";
import { addTimes } from "../utils";

function talkHotdrinkComponent() {
  return component`
    var start, &prevStart, duration = "00:30", &prevDuration, end, name="";

    constraint EndIsStartPlusDur {
      (start, duration -> end) ${(start, duration) =>
        addTimes(start, duration)};
    }

    constraint StartIsPrev {
      (prevStart, prevDuration -> start) ${(prevStart, prevDuration) =>
        addTimes(prevStart, prevDuration)};
    }
  `;
}

function startTalkHotdrinkComponent() {
  return component`
    var start = "09:00", duration = "00:00";
  `;
}

class TalkList extends React.Component {
  constructor(props) {
    super(props);

    // Bind 'this' to all event handlers
    this.updateStartTime = this.updateStartTime.bind(this);
    this.appendNew = this.appendNew.bind(this);
    this.removeEvent = this.removeTalk.bind(this);
    this.moveUp = this.moveUp.bind(this);
    this.updateDuration = this.updateDuration.bind(this);
    this.updateStartTime = this.updateStartTime.bind(this);

    const hdStartTime = startTalkHotdrinkComponent();
    defaultConstraintSystem.addComponent(hdStartTime);

    // Set initial state
    this.state = {
      hdStartTime,
      hdTalks: [],
      startTime: "09:00",
      nameCounter: 0,
    };
  }

  freshName() {
    const current = this.state.nameCounter;
    this.setState({ nameCounter: current + 1 });
    return String.fromCharCode(65 + current); // Start on 'A'
  }

  componentDidMount() {
    const startVariable = this.state.hdStartTime.vs.start;
    startVariable.value.subscribe((change) => {
      if (change.value) {
        this.setState({ startTime: change.value });
      }
    });
  }

  updateDuration(index, value) {
    const newDurations = [...this.state.durations];
    newDurations[index] = value;
    this.setState({ durations: newDurations });
  }

  updateStartTime(event) {
    const newStartTime = event.target.value;
    this.state.hdStartTime.vs.start.value.set(newStartTime);
  }

  // --- The following four functions handle structural operations and are the functions of interest to us --- //
  insertNewAt(index) {
    const newHotdrinkTalk = talkHotdrinkComponent();
    newHotdrinkTalk.vs.name.value.set(this.freshName());
    defaultConstraintSystem.addComponent(newHotdrinkTalk);
    const tempTalkList = [...this.state.hdTalks];
    if (index === 0) {
      newHotdrinkTalk.vs.prevStart = this.state.hdStartTime.vs.start;
      newHotdrinkTalk.vs.prevDuration = this.state.hdStartTime.vs.duration;
    } else {
      newHotdrinkTalk.vs.prevStart = tempTalkList[index - 1].vs.start;
      newHotdrinkTalk.vs.prevDuration = tempTalkList[index - 1].vs.duration;
    }
    if (index < tempTalkList.length) {
      tempTalkList[index].vs.prevStart = this.state.hdStartTime.vs.start;
      tempTalkList[index].vs.prevDuration = this.state.hdStartTime.vs.duration;
    }
    defaultConstraintSystem.update();
    tempTalkList.splice(index, 0, newHotdrinkTalk);
    this.setState({ hdTalks: tempTalkList });
  }

  appendNew() {
    this.insertNewAt(this.state.hdTalks.length);
  }

  removeTalk(index) {
    const tempTalkList = [...this.state.hdTalks];
    let removed;
    // If this is the only item in the list
    if (tempTalkList.length === 1) {
      // do nothing
    }
    // Otherwise, if this was the first element
    else if (index === 0) {
      tempTalkList[1].vs.prevStart = this.state.hdStartTime.vs.start;
      tempTalkList[1].vs.prevDuration = this.state.hdStartTime.vs.duration;
    }
    // Otherwise, if this is not the last element
    else if (index < tempTalkList.length - 1) {
      tempTalkList[index + 1].vs.prevStart = tempTalkList[index - 1].vs.start;
      tempTalkList[index + 1].vs.prevDuration =
        tempTalkList[index - 1].vs.duration;
    }
    removed = tempTalkList.splice(index, 1)[0];
    defaultConstraintSystem.removeComponent(removed);
    defaultConstraintSystem.update();
    this.setState({ hdTalks: tempTalkList });
  }

  moveDown(index) {
    const tempTalkList = [...this.state.hdTalks];
    // If the element is the last in the list
    if (index === tempTalkList.length - 1)
      throw Error("Cannot move last element down");

    tempTalkList[index].vs.prevStart = tempTalkList[index + 1].vs.start;
    tempTalkList[index].vs.prevDuration = tempTalkList[index + 1].vs.duration;

    // If the element we're moving down was the first element
    if (index === 0) {
      tempTalkList[1].vs.prevStart = this.state.hdStartTime.vs.start;
      tempTalkList[1].vs.prevDuration = this.state.hdStartTime.vs.duration;

      // If the element is not the next last element
    } else if (index < tempTalkList.length - 2) {
      tempTalkList[index + 1].vs.prevStart = tempTalkList[index - 1].vs.start;
      tempTalkList[index + 1].vs.prevDuration =
        tempTalkList[index - 1].vs.duration;
    }
    defaultConstraintSystem.update();
    const moved = tempTalkList.splice(index, 1)[0];
    tempTalkList.splice(index + 1, 0, moved);
    this.setState({ hdTalks: tempTalkList });
  }

  moveUp(index) {
    const tempTalkList = [...this.state.hdTalks];
    if (index === 0) throw Error("Cannot move first element up.");
    tempTalkList[index - 1].vs.prevStart = tempTalkList[index].vs.start;
    tempTalkList[index - 1].vs.prevDuration = tempTalkList[index].vs.duration;
    if (index === 1) {
      tempTalkList[index].vs.prevStart = this.state.hdStartTime.vs.start;
      tempTalkList[index].vs.prevDuration = this.state.hdStartTime.vs.duration;
    } else {
      tempTalkList[index].vs.prevStart = tempTalkList[index - 2].vs.start;
      tempTalkList[index].vs.prevDuration = tempTalkList[index - 2].vs.duration;
    }
    if (index < tempTalkList.length - 1) {
      tempTalkList[index + 1].vs.prevStart = tempTalkList[index - 1].vs.start;
      tempTalkList[index + 1].vs.prevDuration =
        tempTalkList[index - 1].vs.duration;
    }
    defaultConstraintSystem.update();
    const moved = tempTalkList.splice(index, 1)[0];
    tempTalkList.splice(index - 1, 0, moved);
    this.setState({ hdTalks: tempTalkList });
  }

  // @Override
  render() {
    const talks = this.state.hdTalks.map((hdTalks, index) => (
      <li key={hdTalks.name}>
        <EventConstraintComponent
          hdComp={hdTalks}
          index={index}
          updateDuration={this.updateDuration}
          remove={this.removeTalk}
          moveUp={this.moveUp}
        />
      </li>
    ));

    return (
      <header className="App-header">
        <h1>With HotDrink</h1>
        <div>
          <input
            onChange={this.updateStartTime}
            type="text"
            value={this.state.startTime}
          />
          <ol>{talks}</ol>
          <button onClick={this.appendNew}>Insert new</button>
        </div>
      </header>
    );
  }
}

export default TalkList;
