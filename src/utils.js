// Time functions are from this Stackoverflow answer:
// https://stackoverflow.com/a/25765236

/** Converts a string in hh:mm format to minutes */
function timeToMins(time) {
  var b = time.split(":");
  return b[0] * 60 + +b[1];
}

/** Convert number of minutes to a string with format hh:mm (range 00-24) */
function timeFromMins(mins) {
  function z(n) {
    return (n < 10 ? "0" : "") + n;
  }
  var h = ((mins / 60) | 0) % 24;
  var m = mins % 60;
  return z(h) + ":" + z(m);
}

/** Add two time strings with format hh:mm */
export function addTimes(t0, t1) {
  return timeFromMins(timeToMins(t0) + timeToMins(t1));
}
