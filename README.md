# The Conference Planner Example in React
*With and without Hotdrink*

### Switching between implementations
Comment/uncomment the react components in `src/App.js` to switch between the implementation that uses
hotdrink and the one that manages dataflow manually.

### Run

To start the application, run `npm install` and `npm start`.
This runs the app in the development mode.
(Open [http://localhost:3000](http://localhost:3000) to view it in the browser.)
The page will reload if you make edits.

Tested with Node v.18.0.1.
